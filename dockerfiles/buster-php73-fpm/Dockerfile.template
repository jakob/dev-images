FROM {{ "buster-php73" | image_tag }} AS buster-php72

# We don't want the default index.html for running PHP applications, and
# xdebug is a major performance drag, so disable it here (it's already
# been disabled for CLI PHP in buster-php72).

RUN {{ "php7.3-fpm" | apt_install }}

# php-fpm configuration:
COPY php-fpm.conf /etc/php/7.3/fpm/php-fpm.conf
RUN cp /docker/www.conf /etc/php/7.3/fpm/pool.d/www.conf

# MediaWiki-specific php.ini settings
# upload_max_filesize and post_max_size here match production, per T246930
RUN echo 'opcache.file_update_protection=0\n\
opcache.memory_consumption=256\n\
opcache.max_accelerated_files=24000\n\
opcache.max_wasted_percentage=10\n\
opcache.fast_shutdown=1\n\
zlib.output_compression=On\n\
upload_max_filesize=100M\n\
post_max_size=100M\n' | tee -a /etc/php/7.3/fpm/php.ini /etc/php/7.3/cli/php.ini

RUN ln -s /docker/20-xdebug.ini "/etc/php/7.3/fpm/conf.d/20-xdebug.ini"

EXPOSE 9000

WORKDIR "/var/www/html/w"

ENTRYPOINT ["/bin/bash", "/php_entrypoint.sh"]
