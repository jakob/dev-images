# releng/dev-images: Docker images for local MediaWiki development

This repo is an attempt to define Docker images to be used for MediaWiki and
services development, both with [local-charts] and Docker Compose environments.

Images here are currently published to the [Wikimedia Docker
registry][registry].  They work in various contexts, but should generally be
understood as experimental.

[local-charts]: https://gerrit.wikimedia.org/r/plugins/gitiles/releng/local-charts/
[registry]: https://tools.wmflabs.org/dockerregistry/

## Prerequisites and installation

You'll need a system with Python 3, [Docker][docker], and [docker-pkg].

You'll probably also want [debchange / dch][dch] for updating changelogs, which
should be available in the `devscripts` package on Debian systems.

[dch]: https://manpages.debian.org/stretch/devscripts/dch.1.en.html
[docker]: https://www.docker.com/
[docker-pkg]: https://doc.wikimedia.org/docker-pkg/

### Installing docker-pkg
```sh
git clone https://gerrit.wikimedia.org/r/operations/docker-images/docker-pkg
pip3 install -e docker-pkg
```

## Building images

Images are built by giving `docker-pkg` a configuration file, the build command,
and a directory of image definitions:

```sh
docker-pkg -c dockerfiles/config.yaml build dockerfiles
```

This will build any image version which doesn't exist in the registry at
docker-registry.wikimedia.org or locally.  If it doesn't have authentication
configured, it won't attempt to push to the registry.

There's a `Makefile` with three targets defined:

  - `make all` will attempt to build everything in `dockerfiles/`
  - `make no-pull` will do the same, but with the `--no-pull` option to
    `docker-pkg` to avoid pulling from the remote registry.  This is useful for
    building images locally for testing.
  - `make use-cache` same as `no-pull`, but reuse cached layers.

### Updating images

If you change an image in any way, be sure to update its `changelog` file with
a new version.

To create new changelog entries for a changed image and its descendants, you
can try something like:

```sh
docker-pkg -c dockerfiles/config.yaml update --reason 'Some change' dev/stretch-php72-fpm-apache2 dockerfiles
```

Be sure to check the results, however.  `docker-pkg` documentation isn't
entirely clear on how this works.

## Deploying images

We use Fabric as a helper for deployment to the Wikimedia Docker registry. It
has to be version 1.x and requires Python 2.7. A virtual environment for
`fabric` is provided through `tox`:

```sh
$ pip install tox
$ tox -e fabric -- deploy_docker
```

### Useful commands

`dch` is available with `apt-get install devscripts` on Debian and related
systems.

```sh
# Increment a changelog file:
dch --increment -c ./dockerfiles/dev-stretch/changelog
```

```sh
# Just build one image - first make sure all versions of the image have been
# deleted from your local registry:
docker-pkg -c dockerfiles/config.yaml build --no-pull --select '*/stretch-php72-fpm-apache2-xdebug:*' dockerfiles
```
